import com.google.zxing.BarcodeFormat
import com.google.zxing.WriterException
import com.google.zxing.client.j2se.MatrixToImageWriter
import com.google.zxing.common.BitMatrix
import com.google.zxing.qrcode.QRCodeWriter
import org.apache.ofbiz.base.util.Debug
import org.apache.ofbiz.base.util.UtilDateTime
import org.apache.ofbiz.base.util.UtilValidate
import org.apache.ofbiz.entity.condition.EntityCondition
import org.apache.ofbiz.entity.condition.EntityOperator
import org.apache.ofbiz.entity.util.EntityUtilProperties
import org.apache.ofbiz.service.ModelService
import org.apache.ofbiz.service.ServiceUtil
import org.influxdb.BatchOptions
import org.influxdb.InfluxDB
import org.influxdb.InfluxDBFactory
import org.influxdb.dto.Point
import org.influxdb.dto.Query

import java.nio.file.FileSystems
import java.nio.file.Path
import java.util.concurrent.TimeUnit

public createDevice() {
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
    uuid = UUID.randomUUID()
    uuidStr = uuid.toString()
    serviceResult = dispatcher.runSync("createProduct", [
            internalName: uuidStr,
            productTypeId: "DEVICE",
            userLogin: systemUserLogin])
    productRole = runService("addPartyToProduct", [productId: serviceResult.productId ,partyId: userLogin.partyId, roleTypeId: "OWNER", userLogin: systemUserLogin])
    result = ServiceUtil.returnSuccess()
    result.deviceId = serviceResult.productId
    return result
}
public getDeviceList() {
    productList = from("Product").where(["productTypeId": "DEVICE"]).queryList()
    deviceList = []
    productList.each { product ->
        status = "available"
        productRole = from("ProductRole").where([roleTypeId: "CUSTOMER", productId: product.productId]).filterByDate().queryList()
        if (productRole) {
            status = "active"
        }
        deviceList.add([
                productId: product.productId,
                internalName: product.internalName,
                productName: product.productName,
                status: status
        ])
    }
    result = ServiceUtil.returnSuccess()
    result.deviceList = deviceList
    return result
}

public getDevice() {
    deviceValue = from("Product").where(["internalName": deviceId]).queryFirst()
    imagePath = System.getProperty("ofbiz.home") + "/framework/images/webapp/images/products/" + deviceId + ".png"
    checkFile = new File(imagePath);
    if (!checkFile.exists()) generateQRCodeImage(deviceId, 350, 350, imagePath)

    result = ServiceUtil.returnSuccess()
    result.deviceId = deviceId
    result.deviceName = deviceValue.productName!=null?deviceValue.productName:"None"
    result.deviceQrUrl = "https://tms-backend.mymemoapps.com/images/products/"  + deviceId + ".png"
    return result
}

public addDevice() {
    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
    product = from("Product").where([internalName: deviceId]).queryFirst()
    if (UtilValidate.isEmpty(product)) {
        return ServiceUtil.returnError("No device from provider.")
    } else {
        productRole = from("ProductRole").where([roleTypeId: "CUSTOMER", productId: product.productId]).filterByDate().queryFirst()
        if (productRole) {
            return ServiceUtil.returnError("This device already added by another one.")
        }
    }

    serviceResult = dispatcher.runSync("updateProduct", [
            productId: product.productId,
            productName: deviceName,
            userLogin: systemUserLogin])
    productRole = runService("addPartyToProduct", [productId: product.productId, partyId: userLogin.partyId, roleTypeId: "CUSTOMER", userLogin: systemUserLogin])
    result = ServiceUtil.returnSuccess()
    result.deviceId = deviceId
    return result
}
public removeDevice() {
    product = from("Product").where([internalName: deviceId]).queryFirst()
    productRole = from("ProductRole").where([
            roleTypeId: "CUSTOMER",
            productId: product.productId,
            partyId: userLogin.partyId
    ]).filterByDate().queryFirst()
    if (productRole) {
        productRole.thruDate = UtilDateTime.nowTimestamp()
        productRole.store()
    }
    return ServiceUtil.returnSuccess("Your device [ " + product.productName + "] has been removed.")
}

public getCustomerDeviceList() {
    productRoleList = from("ProductRole").where(["partyId": userLogin.partyId, roleTypeId: "CUSTOMER"]).filterByDate().queryList()
    productIds = new HashSet<String>()
    productRoleList.each { productRole ->
        productIds.add(productRole.productId)
    }
    deviceList = from("Product").where(EntityCondition.makeCondition("productId",
    EntityOperator.IN, productIds)).orderBy("productName").queryList()
    result = ServiceUtil.returnSuccess()
    result.deviceList = deviceList
    return result
}
public storeTemperature() {
    try {
        url = EntityUtilProperties.getPropertyValue("general", "influxdbURL", "", delegator)
        dbName = EntityUtilProperties.getPropertyValue("general", "influxdbName", "", delegator)
        username = EntityUtilProperties.getPropertyValue("general", "influxdbUsername", "", delegator)
        password = EntityUtilProperties.getPropertyValue("general", "influxdbPassword", "", delegator)
        Debug.log("===> storeTemperature...")
        Debug.log("- deviceId: " + deviceId)
        Debug.log("- temperature: " + temperature)
        Debug.log("- url: " + url)
        Debug.log("- dbName: " + dbName)
        Debug.log("- username: " + username)
        Debug.log("- password: " + password)
        InfluxDB influxDB = InfluxDBFactory
                .connect(url ,username, password)
        influxDB.setDatabase(dbName)
        influxDB.setRetentionPolicy("autogen")
        influxDB.enableBatch(BatchOptions.DEFAULTS)

        influxDB.write(Point.measurement("temperature")
                .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
                .addField("deviceId", deviceId)
                .addField("value", temperature)
                .build())
        influxDB.close()
        // Check and send notification
        product = from("Product").where([internalName: deviceId]).queryFirst()
        productNotificationStatus = from("ProductAttribute")
            .where([productId: product.productId, attrName: "notificationStatus", attrValue: "ON"]).queryFirst()
        Debug.log("- productNotificationStatus: " + productNotificationStatus)
        if (UtilValidate.isNotEmpty(productNotificationStatus)) {
            productNotificationTime = from("ProductAttribute")
                    .where([productId: product.productId, attrName: "notificationTime"]).queryFirst()
            // Check Repeat
            repeatable = true;
            if (productNotificationTime) {
                lastTime = UtilDateTime.getTimestamp(Long.parseLong(productNotificationTime.attrValue)).getTime()
                notificationRepeat = from("ProductAttribute")
                        .where([productId: product.productId, attrName: "notificationRepeat"]).queryFirst()
                if (notificationRepeat) {
                    repeat = Long.parseLong(notificationRepeat.attrValue)
                    repeatTime = repeat * 60 * 1000
                    diffTime = UtilDateTime.nowTimestamp().getTime() - lastTime
                    if (diffTime < repeatTime) {
                        repeatable = false
                    }
                    Debug.log("r: " + repeatTime)
                    Debug.log("d: " + diffTime)
                    Debug.log("repeatable: " + repeatable)
                }

            }
            if (UtilValidate.isEmpty(productNotificationTime) || repeatable) {
                productNotificationMax = from("ProductAttribute")
                        .where([productId: product.productId, attrName: "notificationMax"]).queryFirst()
                max = productNotificationMax.attrValue
                productNotificationMax = from("ProductAttribute")
                        .where([productId: product.productId, attrName: "notificationMin"]).queryFirst()
                min = productNotificationMax.attrValue

                temperatureInt = Integer.parseInt(temperature)
                maxInt = Integer.parseInt(max)
                minInt = Integer.parseInt(min)
                if (minInt <= temperatureInt && maxInt >= temperatureInt) {
                    Debug.log("- condition true")
                    message = "Temperature of Device [ " + product.productName + " ] is " + temperature + " °C"
                    // Find partyId to send
                    productRoleList = from("ProductRole")
                            .where([productId: product.productId, roleTypeId: "CUSTOMER"]).filterByDate().queryList()
                    systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
                    productRoleList.each { productRole ->
                        userLogin = from("UserLogin").where([partyId: productRole.partyId]).queryFirst()
                        Debug.log("- send email: " + userLogin.userLoginId)
                        emailTemplateSetting = from("EmailTemplateSetting").where("emailTemplateSettingId", "DEVICE_TEMP_NOTIFY").queryOne();
                        serviceContext = [:]
                        serviceContext.put("bodyScreenUri", emailTemplateSetting.getString("bodyScreenLocation"))
                        serviceContext.put("bodyParameters", [message: message])
                        serviceContext.put("subject", emailTemplateSetting.getString("subject"))
                        serviceContext.put("sendFrom", emailTemplateSetting.get("fromAddress"))
                        serviceContext.put("sendTo", userLogin.userLoginId)
                        serviceContext.put("partyId", userLogin.partyId)
                        serviceResult = runService("sendMailHiddenInLogFromScreen", serviceContext);
                        productNotificationTime = from("ProductAttribute")
                                .where([productId: product.productId, attrName: "notificationTime"]).queryFirst()
                        if (productNotificationTime) {
                            productNotificationTime.attrValue = UtilDateTime.nowTimestamp().getTime() + ""
                            productNotificationTime.store();
                        } else {
                            runService("createProductAttribute", [productId: product.productId, attrName: "notificationTime", attrValue: (UtilDateTime.nowTimestamp().getTime() + ""), userLogin: systemUserLogin])
                        }
                    }
                }
            }
        }
    } catch (Exception e) {
        Debug.logError(e, null);
    }
    result = ServiceUtil.returnSuccess()
    return result
}
public getDeviceTemperatureList() {
    result = ServiceUtil.returnSuccess()
    try {
        url = EntityUtilProperties.getPropertyValue("general", "influxdbURL", "", delegator)
        dbName = EntityUtilProperties.getPropertyValue("general", "influxdbName", "", delegator)
        username = EntityUtilProperties.getPropertyValue("general", "influxdbUsername", "", delegator)
        password = EntityUtilProperties.getPropertyValue("general", "influxdbPassword", "", delegator)

        InfluxDB influxDB = InfluxDBFactory
                .connect(url
                , username
                , password)
        Query query = new Query("SELECT * FROM temperature WHERE (deviceId = '" + deviceId + "') AND time >= now() - " + duration
                , dbName)
        qResult = influxDB.query(query)
        times = []
        values = []
        min = 0
        max = 100
        index = 0
        if (qResult.getResults().size() > 0) {
            qResult.getResults().get(0).getSeries().get(0).values.each { value ->
                times.add(value.get(0))
                values.add(value.get(2))
                valueInt = Integer.parseInt(value.get(2))
                if (index == 0) {
                    min = valueInt
                    max = valueInt
                } else {
                    if (valueInt < min) {
                        min = valueInt
                    }
                    if (valueInt > max) {
                        max = valueInt
                    }
                }
                index++
            }
        }
        result.times = times
        result.values = values
        result.min = min
        result.max = max
        influxDB.close()
    } catch (Exception e) {
        Debug.logError(e, null);
    }
    return result
}

public updateDeviceNotification() {
    try {
        systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
        product = from("Product").where([internalName: deviceId]).queryFirst()
        productId = product.productId;

        notificationStatus = from("ProductAttribute")
                .where([attrName: "notificationStatus", productId: productId]).queryFirst()
        if (notificationStatus) {
            notificationStatus.attrValue = status
            notificationStatus.store()
        } else {
            runService("createProductAttribute", [productId: productId, attrName: "notificationStatus", attrValue: status, userLogin: systemUserLogin])
        }

        notificationMax = from("ProductAttribute")
                .where([attrName: "notificationMax", productId: productId]).queryFirst()
        if (notificationMax) {
            notificationMax.attrValue = max + ""
            notificationMax.store()
        } else {
            runService("createProductAttribute", [productId: productId, attrName: "notificationMax", attrValue: max, userLogin: systemUserLogin])
        }

        notificationMin = from("ProductAttribute")
                .where([attrName: "notificationMin", productId: productId]).queryFirst()
        if (notificationMin) {
            notificationMin.attrValue = min + ""
            notificationMin.store()
        } else {
            runService("createProductAttribute", [productId: productId, attrName: "notificationMin", attrValue: min, userLogin: systemUserLogin])
        }

        notificationRepeat = from("ProductAttribute")
                .where([attrName: "notificationRepeat", productId: productId]).queryFirst()
        if (notificationRepeat) {
            notificationRepeat.attrValue = repeat + ""
            notificationRepeat.store()
        } else {
            runService("createProductAttribute", [productId: productId, attrName: "notificationRepeat", attrValue: repeat, userLogin: systemUserLogin])
        }

    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Update notification service is missing.")
    }
    return ServiceUtil.returnSuccess()
}

public getDeviceNotification() {
    try {
        systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne()
        product = from("Product").where([internalName: deviceId]).queryFirst()
        productId = product.productId;
        status = "OFF"
        max = "100"
        min = "0"
        repeat = "10"
        notificationStatus = from("ProductAttribute")
                .where([attrName: "notificationStatus", productId: productId]).queryFirst()
        if (UtilValidate.isEmpty(notificationStatus)) {
            runService("createProductAttribute", [productId: productId, attrName: "notificationStatus", attrValue: status, userLogin: systemUserLogin])
        } else {
            status = notificationStatus.attrValue
        }

        notificationMax = from("ProductAttribute")
                .where([attrName: "notificationMax", productId: productId]).queryFirst()
        if (UtilValidate.isEmpty(notificationMax)) {
            runService("createProductAttribute", [productId: productId, attrName: "notificationMax", attrValue: max, userLogin: systemUserLogin])
        } else {
            max = notificationMax.attrValue
        }

        notificationMin = from("ProductAttribute")
                .where([attrName: "notificationMin", productId: productId]).queryFirst()
        if (UtilValidate.isEmpty(notificationMin)) {
            runService("createProductAttribute", [productId: productId, attrName: "notificationMin", attrValue: min, userLogin: systemUserLogin])
        } else {
            min = notificationMin.attrValue
        }

        notificationRepeat = from("ProductAttribute")
                .where([attrName: "notificationRepeat", productId: productId]).queryFirst()
        if (UtilValidate.isEmpty(notificationRepeat)) {
            runService("createProductAttribute", [productId: productId, attrName: "notificationRepeat", attrValue: repeat, userLogin: systemUserLogin])
        } else {
            repeat = notificationRepeat.attrValue
        }
        result = ServiceUtil.returnSuccess();
        result.status = status
        result.max = Integer.parseInt(max)
        result.min = Integer.parseInt(min)
        result.repeat = Integer.parseInt(repeat)
    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Update notification service is missing.")
    }
    return result
}
public updateDevice() {
    try {
        product = from("Product").where([internalName: deviceId]).queryFirst()
        product.productName = deviceName
        product.store()
    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Update device service is missing.")
    }
    return ServiceUtil.returnSuccess("Update device successful.")
}

/* Utilities */
void generateQRCodeImage(String text, int width, int height, String filePath)
        throws WriterException, IOException {
    QRCodeWriter qrCodeWriter = new QRCodeWriter()
    BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height)
    Path path = FileSystems.getDefault().getPath(filePath)
    MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path)
}

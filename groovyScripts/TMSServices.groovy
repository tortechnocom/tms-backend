import org.apache.ofbiz.base.util.Debug
import org.apache.ofbiz.entity.condition.EntityCondition
import org.apache.ofbiz.entity.condition.EntityOperator
import org.apache.ofbiz.service.ServiceUtil

public getGlobal() {
    try {
        systemUserLogin = from("UserLogin").where([userLoginId: "system"]).queryOne();
        productList = from("Product").where(["productTypeId": "DEVICE"]).queryList()
        productRoleList = from("ProductRole").where([ roleTypeId: "CUSTOMER"]).filterByDate().queryList()
        productIds = new HashSet<String>()
        productRoleList.each { productRole ->
            productIds.add(productRole.productId)
        }
        deviceList = from("Product").where(EntityCondition.makeCondition("productId",
                EntityOperator.IN, productIds)).orderBy("productName").queryList()
        // get newsList
        newsList = select("contentId", "contentName", "createdDate").from("Content").where(["contentTypeId": "NEWS", "localeString" : language]).queryList()

        result = ServiceUtil.returnSuccess()
        result.activeDevice = productRoleList.size()
        result.availableDevice = productList.size() - productRoleList.size()
        result.newsList = newsList
        return result
    } catch (Exception e) {
        Debug.logError(e, null);
        return ServiceUtil.returnError("Get Global info is missing.")
    }
}
